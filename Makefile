DOCKER_COMPOSE=docker-compose
APP_CONTAINER=app

DEFAULT_GOAL := help
help:
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z0-9_-]+:.*?##/ { printf "  \033[36m%-27s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

##@ [Docker] Build / Infrastructure

.PHONY: di
di: .env ## Make sure the .env file exists for docker

.PHONY: dps
dps: di ## Show all containers
	$(DOCKER_COMPOSE) ps

.PHONY: dex
dex: di ## Exec command inside php_fpm container. For command use c=<command>
	$(DOCKER_COMPOSE) exec $(APP_CONTAINER) $(c)

.PHONY: build
build: di ## Build dev docker environment
	@docker-compose -f docker-compose.yml build

.PHONY: build-test
build-test: di ## Build test docker environment
	@docker-compose -f docker-compose-test-ci.yml build

.PHONY: build-staging
build-staging: di ## Build staging docker environment
	@docker-compose -f docker-compose-staging.yml build

.PHONY: build-prod
build-prod: di ## Build prod docker environment
	@docker-compose -f docker-compose-prod.yml build

.PHONY: start
start: di ## Start dev docker containers
	@docker-compose -f docker-compose.yml up -d

.PHONY: start-test
start-test: di ## Start test docker containers
	@docker-compose -f docker-compose-test-ci.yml up -d

.PHONY: start-staging
start-staging: di ## Start staging docker containers
	@docker-compose -f docker-compose-staging.yml up -d

.PHONY: start-prod
start-prod: di ## Start prod docker containers
	@docker-compose -f docker-compose-prod.yml up -d

.PHONY: stop
stop: di ## Stop dev docker containers
	@docker-compose -f docker-compose.yml down

.PHONY: stop-test
stop-test: di ## Stop test docker containers
	@docker-compose -f docker-compose-test-ci.yml down

.PHONY: stop-staging
stop-staging: di ## Stop staging docker containers
	@docker-compose -f docker-compose-staging.yml down

.PHONY: stop-prod
stop-prod: di ## Stop prod docker containers
	@docker-compose -f docker-compose-prod.yml down

.PHONY: restart
restart: stop start ## Restart dev docker containers

.PHONY: restart-test
restart-test: stop-test start-test ## Restart test docker containers

.PHONY: restart-staging
restart-staging: stop-staging start-staging ## Restart staging docker containers

.PHONY: restart-prod
restart-prod: stop-prod start-prod ## Restart prod docker containers

##@ [Laravel] Laravel commands
.PHONY: a
a: ## php artisan. c=<command>
	$(DOCKER_COMPOSE) exec $(APP_CONTAINER) php artisan $(c)

.PHONY: migrate
migrate: ## php artisan migrate
	$(DOCKER_COMPOSE) exec $(APP_CONTAINER) php artisan migrate

.PHONY: routes
routes: ## php artisan route:list
	$(DOCKER_COMPOSE) exec $(APP_CONTAINER) php artisan route:list

.PHONY: tinker
tinker: ## php artisan tinker
	$(DOCKER_COMPOSE) exec $(APP_CONTAINER) php artisan tinker

.PHONY: seed
seed: ## php artisan db:seed
	$(DOCKER_COMPOSE) exec $(APP_CONTAINER) php artisan db:seed

.PHONY: swagger
swagger: ## php artisan l5-swagger:generate
	$(DOCKER_COMPOSE) exec $(APP_CONTAINER) php artisan l5-swagger:generate

.PHONY: passport
passport: ## php artisan passport:install
	$(DOCKER_COMPOSE) exec $(APP_CONTAINER) php artisan passport:install

.PHONY: terminate
terminate: ## php artisan horizon:terminate
	$(DOCKER_COMPOSE) exec $(APP_CONTAINER) php artisan horizon:terminate

##@ [Laravel] Laravel Enums
.PHONY: enum
enum: ## php artisan make:enum. n=<name>
	$(DOCKER_COMPOSE) exec $(APP_CONTAINER) php artisan make:enum $(n)

##@ [Laravel] Laravel Ide Helper
.PHONY: ihg
ihg: ## php artisan ide-helper:generate
	$(DOCKER_COMPOSE) exec $(APP_CONTAINER) php artisan ide-helper:generate

.PHONY: ihm
ihm: ## php artisan ide-helper:models. m=<model>
	$(DOCKER_COMPOSE) exec $(APP_CONTAINER) php artisan ide-helper:models $(m)


##@ [Lucid] Lucid commands
.PHONY: lc
lc: ## Create a Controller. n=<name>, s=<service>
	$(DOCKER_COMPOSE) exec $(PHP_FPM_CONTAINER) ./vendor/bin/lucid make:controller $(n) $(s)

.PHONY: ls
ls: ## Create a Service. For service name use n=<service>
	$(DOCKER_COMPOSE) exec $(PHP_FPM_CONTAINER) ./vendor/bin/lucid make:service $(n)

.PHONY: lf
lf: ## Create a Feature. For feature name use n=<name> s=<service>
	$(DOCKER_COMPOSE) exec $(PHP_FPM_CONTAINER) ./vendor/bin/lucid make:feature $(n) $(s)

.PHONY: lj
lj: ## Create a Job. For job name use n=<name>, job domain d=<domain>
	$(DOCKER_COMPOSE) exec $(PHP_FPM_CONTAINER) ./vendor/bin/lucid make:job $(n) $(d)
