#!/bin/bash

cd ..;

# Copy docker environment file
if [ ! -f .env ]; then
  cp .env.example .env
  echo -e "\e[32m.env added"
else
  echo -e "\e[93mDocker .env exist\e[49m"
fi

# Export the vars in .env into shell:
export $(egrep -v '^#' .env | xargs);

# Create docker network if doesn't exist
if [[ ! $(docker network inspect $(docker network ls -q) | grep ${NETWORK}) ]]; then
  docker network create ${NETWORK}
fi

# Copy laravel .env from .env.example if doesn't exist
if [ ! -f ./src/.env ]; then
  cp ./src/.env.example ./src/.env
  echo -e "\e[32mapi/.env added"
else
  echo -e "\e[93mAPI .env exist\e[49m"
fi

# Run docker containers
docker-compose up -d

# Laravel setup
docker-compose exec app composer install && \
  docker-compose exec app php artisan key:generate && \
  docker-compose exec app php artisan storage:link -q && \
  docker-compose exec app chmod -R 775 storage && \
  docker-compose exec app chmod -R 775 bootstrap/cache && \
  docker-compose exec app php artisan migrate --force