# Optima

Folder structure and technologies in use:

- Laravel 7.0, MariaDB 10.4

## Table of contents

- [Prerequisites](#prerequisites)
- [Get the source code](#get-the-source-code)

## Prerequisites

- Docker >= 19.03.11
- Docker Compose >= 1.22

## Get the Source Code

Clone the repository using the following command:

```
git clone git@git.bvblogic.tools:backend/Optima-doker.git
```

## Development


For your dev environment you can use either **1. Bash script** or **2. Manual** setup

## 1. Bash script setup

```bash
cd _setup && ./dev.sh
```

## 2. Manual setup

### 1. Copy .env files

```bash
cp .env.example .env && cp ./src/.env.example ./src/.env
```

### 2. Create docker network and volume

```bash
docker network create dsylectus
```

### 3. Start docker services

```bash
make build
make start
```

or

```bash
docker-compose up -d --build
```

### 4. Configure PHP app

```bash
make dex c="php artisan key:generate && php artisan storage:link -q"
```

or

```bash
docker-compose app exec php artisan key:generate && php artisan storage:link -q
```

### 6. Run the migrations

```bash
make migrate
```

or

```bash
docker-compose app exec php artisan migrate
```

## Additional main command available

To see all commands list just type:
```bash
make
```